: << SPRAVKA
Создание бинарного дерева
Добавление узла
Удаление узла. С двумя ребрами заменяем на наименьшее значение из правого поддерева
Разобраться с удалением корня
SPRAVKA

#!/usr/bin/env bash

base=arr
count=0
root=${base}$count

function create { #создаем псевдо структуру
	node=${base}$count
	eval $node[0]=$1 #ключ
	eval $node[1]=nil #левое плечо
	eval $node[2]=nil #правое плечо
	eval $node[3]=nil #родительское ребро
	eval $node[4]=\"$2\" #необязательное поле для заметки или данные
	((count++))
}

function add {
	local tmp=$root
	while [ 1 ]; do 
#		if [ $1 -eq $(eval echo \$$tmp) ]; then #тоже что и \${$tmp[0]}
#			return
#		fi
		if [ $1 -ge $(eval echo \$$tmp) ]; then #\$$tmp == \${$tmp[0]}
			if [ $(eval echo \${$tmp[2]}) == nil ]; then
				create $1 "$2"
				eval $tmp[2]=$node
				eval $node[3]=$tmp
				return
			else
				tmp=$(eval echo \${$tmp[2]})
			fi
		else
			if [ $(eval echo \${$tmp[1]}) == nil ]; then
				create $1 "$2"
				eval $tmp[1]=$node
				eval $node[3]=$tmp
				return
			else
				tmp=$(eval echo \${$tmp[1]})
			fi
		fi
	done
}

function search {
	local tmp=$root
	while [ 1 ]; do
		if [ $1 -eq $(eval echo \$$tmp) ]; then
			echo $tmp	
			return
		elif [ $1 -gt $(eval echo \$$tmp) ]; then
			if [ $(eval echo \${$tmp[2]}) == nil ]; then
				echo nil
				return
			fi
			tmp=$(eval echo \${$tmp[2]})
		else
			if [ $(eval echo \${$tmp[1]}) == nil ]; then
				echo nil
				return
			fi
			tmp=$(eval echo \${$tmp[1]})
		fi
	done
}

function min {
	local tmp=$1
	while [ 1 ]; do
		if [ $(eval echo \${$tmp[1]}) == nil ]; then
			echo $tmp
			return
		else
			tmp=$(eval echo \${$tmp[1]})
		fi
	done
}

function delit {
	local del=$(search $1)
	if [ $del == nil ]; then
		echo "нет такого элемента"
		return
	fi
	local prev=$(eval echo \${$del[3]})
	if [ $prev == nil ]; then
		return #заглушка на удаление корня
	fi
	if [ $(eval echo \${$del[1]}) == nil ]; then
		if [ $(eval echo \${$del[2]}) == nil ]; then
			if [ $(eval echo \$$del) -lt $(eval echo \$$prev) ]; then
				eval $prev[1]=nil
			else
				eval $prev[2]=nil
			fi
		else
			local next=$(eval echo \${$del[2]})
			if [ $(eval echo \$$del) -lt $(eval echo \$$prev) ]; then
				eval $prev[1]=$next
			else
				eval $prev[2]=$next
			fi
			eval $next[3]=$prev
		fi
	else
		if [ $(eval echo \${$del[2]}) == nil ]; then
			local next=$(eval echo \${$del[1]})
			if [ $(eval echo \$$del) -lt $(eval echo \$$prev) ]; then
				eval $prev[1]=$next
			else
				eval $prev[2]=$next
			fi
			eval $next[3]=$prev
		else
			local ins=$(min $(eval echo \${$del[2]}))
			eval $del[0]=$(eval echo \$$ins)
			eval $del[4]=\"$(eval echo \${$ins[4]})\"
			local end=$(eval echo \${$ins[2]})
			if [ $(eval echo \${$ins[3]}) == $del ]; then
				eval $del[2]=$end
				if [ $end != nil ]; then
					eval $end[3]=$del
				fi
			else
				local tmp=$(eval echo \${$ins[3]})
				eval $tmp[1]=$end
				if [ $end != nil ]; then
					eval $end[3]=$tmp
				fi
			fi
			del=$ins

		fi
	fi
	unset $del
}

function tprint {
	if [ $1 ]; then
		tprint $(eval echo \${$1[1]})
		eval echo -n \$$1 \${$1[4]} #\${$1[0]}
#		eval echo -n \$$1
		echo -n " "
		tprint $(eval echo \${$1[2]})
	fi
}

function radd {
	while [ $1 ]; do
		add $1 "$2"
		tprint $root
		echo
		shift 2
	done
}

function redel {
	while [ $1 ]; do
		delit $1
		tprint $root
		echo
		shift
	done
}

create 10 ten		#создаем корень дерева
echo -n " "
eval echo \$$root \${$root[4]}
#add 12 twelve		#добавляем элементы
#tprint $root		#выводим на экран
#echo
#delit 14 #удаляем узел из середины с двумя ребрами
radd 12 twelve 9 '' 7 '' 2 '' 4 '' 14 fourteen 9 '' 13 thirteen 8 '' -1 "minus one" 0 zero 100 hundred 66 "sixty six" 105 "bin tree" 67 '' 68 ''
redel 14 100 66 0 -1 9 7 12 67 4 8 105 2 68 13 9
