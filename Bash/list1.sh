: << SPRAVKA
Гибрид, двусвязный список
с сортировкой и
головой в начале
c наименьшим ключем
SPRAVKA

#!/usr/bin/env bash

base="arr"
count=0
root="${base}$count"
#node=""

function create { #создаем структуру
	node=${base}$count
	eval $node[0]=$1 #данные
	eval $node[1]="nil" #левое плечо,родительское
	eval $node[2]="nil" #правое плечо,дочернее
	if [ "$2" ]; then
		eval $node[3]=\"$2\" #дополнительное поле
	else
		eval $node[3]="nil"
	fi
	((count++))
}

function add {
	local flag= #смена направления движения по списку
	local tmp=$root
	while [ 1 ]; do
		if [ $1 -eq $(eval echo \$$tmp) ]; then
			return
		elif [ $1 -gt $(eval echo \$$tmp) ]; then
			if [ $(eval echo \${$tmp[2]}) == "nil" ]; then
				create $1
				eval $node[1]=$tmp
				eval $tmp[2]=$node
				return
			else
				flag=1
				tmp=$(eval echo \${$tmp[2]})
			fi
		else
			create $1
			if [ $flag ]; then #если вставляем в середину
				prev=$(eval echo \${$tmp[1]})
				eval $node[1]=$prev
				eval $prev[2]=$node
			else
				root=$node #переносим точку входа на начало списка
			fi
			eval $node[2]=$tmp
			eval $tmp[1]=$node
			return
		fi
	done
}

function lprint {
	if [ $1 ]; then
		eval echo -n \$$1
		echo -n ' '
		lprint $(eval echo \${$1[2]})
	fi
}

create 10 ten
add 10
add 12
add 14
add 8
add 6
add 6
add 12
add 7
add 100
add -66
add 13

lprint $root
echo ""
