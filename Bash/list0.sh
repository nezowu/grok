: << SPRAVKA
Двусвязный список
с добавлением в конец
SPRAVKA

#!/usr/bin/env bash

base="arr"
count=0
root="${base}$count"
#node=""

function create { #создаем структуру
	node=${base}$count
	eval $node[0]=$1 #данные
	eval $node[1]="nil" #левое плечо,родительское
	eval $node[2]="nil" #правое плечо,дочернее
	if [ "$2" ]; then
		eval $node[3]=\"$2\" #дополнительное поле
	else
		eval $node[3]="nil"
	fi
	((count++))
}

function add {
	local tmp=$root
	while [ 1 ]; do
		if [ $(eval echo \${$tmp[2]}) == "nil" ]; then
			create $1 "$2"
			eval $node[1]=$tmp
			eval $tmp[2]=$node
			return
		fi
			tmp=$(eval echo \${$tmp[2]})
	done
}

function lprint {
	if [ $1 ]; then
		eval echo -n "\$$1 \${$1[3]}"
		echo -n ' '
		lprint $(eval echo \${$1[2]})
	fi
}

create 10 ten
add 10 "else ten"
add 12 twelve
add 6 six
add 6 "another six"
add 12 "twelve else one"
add 100 hundred
add -66 "minus sixty six"
add 13 thirteen

lprint $root
echo ""
